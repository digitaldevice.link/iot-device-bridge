FROM scratch
COPY target/x86_64-unknown-linux-musl/release/iot_device_bridge /
COPY deployment_package/device-iot.config /device-iot.config
CMD ["/iot_device_bridge"]
