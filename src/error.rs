//! IoTError utilities for abstracting and alignment of errors from used crates.

use rumqttc::{ClientError, ConnectionError};
use std::fmt;
use std::fmt::Display;

#[derive(Debug, PartialEq)]
pub enum IoTError {
    AWSConnectionError,
    AWSClientError,
    AWSCreateCertificateFromCsrError,
    AWSCreateCsrError,
    AWSRegisterThingError,
    AWSResponseError,
    DeviceAdapterError,
    DeviceConnectionError,
    DeviceShadowError,
    MqttError,
    NotDeviceShadowTopicError,
    IotStatusParsingError,
    IoError,
    MultitaskingError,
    ChannelSendError,
    ECIESError, 
    SerializeError,
    UnknownIotError,
}

impl Display for IoTError {
    fn fmt(&self, f: &mut fmt::Formatter) -> std::fmt::Result {
        match self {
            IoTError::AWSConnectionError => write!(f, "AWS connecting error"),
            IoTError::AWSClientError => write!(f, "AWS MQTT client error"),
            IoTError::AWSCreateCertificateFromCsrError => {
                write!(f, "Error response to AWS CreateCertificateFromCsr")
            }
            IoTError::AWSCreateCsrError => write!(f, "Error creating CSR for AWS"),
            IoTError::AWSRegisterThingError => write!(f, "Error response to AWS RegisterThing"),
            IoTError::AWSResponseError => write!(f, "Error in AWS response"),
            IoTError::DeviceAdapterError => write!(f, "Error in DeviceAdapter"),
            IoTError::DeviceConnectionError => write!(f, "Device connecting error"),
            IoTError::DeviceShadowError => write!(f, "Error response from DeviceShadow"),
            IoTError::MqttError => write!(f, "Error from MQTT client"),
            IoTError::NotDeviceShadowTopicError => write!(f, "Not DeviceShadow Topic error"),
            IoTError::IotStatusParsingError => write!(f, "IoT Registration Status Parsing Error"),
            IoTError::IoError => write!(f, "Error I/O"),
            IoTError::MultitaskingError => write!(f, "Error in tokio task join"),
            IoTError::ChannelSendError => write!(f, "Error in tokio channel send"),
            IoTError::SerializeError => write!(f, "Error serializing to JSON or YAML"),
            IoTError::ECIESError => write!(f, "Error in ECIES encryption"),
            IoTError::UnknownIotError => write!(f, "Unknown IoT Error"),
        }
    }
}

impl From<std::io::Error> for IoTError {
    fn from(_err: std::io::Error) -> IoTError {
        IoTError::IoError
    }
}

impl From<serde_json::Error> for IoTError {
    fn from(_err: serde_json::Error) -> IoTError {
        IoTError::SerializeError
    }
}

impl From<serde_yaml::Error> for IoTError {
    fn from(_err: serde_yaml::Error) -> IoTError {
        IoTError::SerializeError
    }
}

impl From<ConnectionError> for IoTError {
    fn from(_err: ConnectionError) -> IoTError {
        IoTError::AWSConnectionError
    }
}

impl From<ClientError> for IoTError {
    fn from(_err: ClientError) -> IoTError {
        IoTError::AWSClientError
    }
}

impl From<openssl::error::ErrorStack> for IoTError {
    fn from(_err: openssl::error::ErrorStack) -> IoTError {
        IoTError::AWSCreateCsrError
    }
}

impl From<tokio::task::JoinError> for IoTError {
    fn from(_err: tokio::task::JoinError) -> IoTError {
        IoTError::MultitaskingError
    }
}

impl<T> From<tokio::sync::mpsc::error::SendError<T>> for IoTError {
    fn from(_err: tokio::sync::mpsc::error::SendError<T>) -> IoTError {
        IoTError::ChannelSendError
    }
}

impl From<std::string::String> for IoTError {
    fn from(_err: std::string::String) -> IoTError {
        IoTError::UnknownIotError
    }
}

impl From<ecies::SecpError> for IoTError {
    fn from(_err: ecies::SecpError) -> IoTError {
        IoTError::ECIESError
    }
}

impl From<rumqttc::mqttbytes::Error> for IoTError {
    fn from(_err: rumqttc::mqttbytes::Error) -> IoTError {
        IoTError::MqttError
    }
}
