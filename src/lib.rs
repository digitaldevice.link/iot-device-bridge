//! The `iot_device_bridge` implements the most important functions of AWS IoT device SDK
//! to connect to the AWS IOT Core and other alternate MQTT IoT (non AWS).
//!
//! Implemented IoT functionality:
//!   - Connecting to AWS IoT Core via MQTT -- sending / receiving messages
//!   - AWS Fleet Provisioning using the CreateCertificateFromCsr mode
//!   - AWS Device Shadow with 2 shadows
//!      - IoT Shadow for managing of the IoT connection and
//!      - Device Shadow for configuring the filtering and mapping rules
//!
//! For details regarding the software design see the [README.md](https://gitlab.com/digitaldevice.link/iot-device-bridge/-/blob/main/README.md) in the code repository.
//! 
//! For details regarding use in the code see the structure and comments in the [`main.rs`](https://gitlab.com/digitaldevice.link/iot-device-bridge/-/blob/main/src/main.rs).
//!
pub mod config;
pub mod connector_aws;
pub mod crypto;
pub mod device_adapter;
pub mod device_shadow;
pub mod error;
pub mod fleet_provisioning;
pub mod mqtt_client;
