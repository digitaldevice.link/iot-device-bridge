//! The `iot_device_bridge` implements the most important functions of AWS IoT device SDK
//! to connect to the AWS IOT Core and other alternate MQTT IoT (non AWS).
//!
//! Implemented IoT functionality:
//!   - Connecting to AWS IoT Core via MQTT -- sending / receiving messages
//!   - AWS Fleet Provisioning using the CreateCertificateFromCsr mode
//!   - AWS Device Shadow with 2 shadows
//!      - IoT Shadow for managing of the IoT connection and
//!      - Device Shadow for configuring the filtering and mapping rules
//!
//! For details regarding the software design see the [README.md](https://gitlab.com/digitaldevice.link/iot-device-bridge/-/blob/main/README.md) in the code repository.
//! 
extern crate iot_device_bridge;

use iot_device_bridge::config::{self, Config, IotRegistrationStatus};
use iot_device_bridge::connector_aws::{self, IotMessage, IotState};
use iot_device_bridge::device_adapter::{self, DeviceState};
use iot_device_bridge::device_shadow::{self, DeviceShadowDescriptor, ShadowType};
use iot_device_bridge::error::IoTError;
use iot_device_bridge::fleet_provisioning;
use iot_device_bridge::mqtt_client::{self, AsyncClient, ConnectionSettings};
use rumqttc::{self, Packet, QoS};

use tokio::sync::broadcast::{self, Receiver as Rx, Sender as Tx};
use tokio::sync::mpsc::{self, Receiver, Sender};
use tokio::time::Duration;
use tokio_retry::strategy::FibonacciBackoff;
use tokio_retry::Retry;

use std::sync::{Arc, Mutex};

use ignore_result::Ignore;
use log::{debug, warn, LevelFilter};
use simple_logger::SimpleLogger;

const CHANNEL_CAPACITY: usize = 32;
const DEVICE_SUB_TOPIC: &str = "#";

// MQTT Bridge function implementing the main workflow of bridge, fleet provisioning and device shadows
async fn mqtt_bridge() -> Result<IotRegistrationStatus, IoTError> {
    // Initiate general functionality
    let mut config = Config::get_config_from_yaml()?;
    
    // New iot_state  defaults to IotRegistrationStatus::Registered
    let iot_state = IotState::new();
    let iot_state_1 = Arc::new(Mutex::new(iot_state));
    let iot_state_2 = Arc::clone(&iot_state_1);
    iot_state_1.lock().unwrap().set_iot_registration_status(IotRegistrationStatus::Registered);

    // Variable for deciding to *upload* the IoT Shadow to sync the Registration Status
    // The IoT Shadow upload should happen as effect of the fleet provisioning
    // Empty string means no update
    let mut iot_shadow_initial_upload = String::new();
    // Initiate Device State and prepare for asynchronous multitasking
    let mut device_shadow_initial_upload = String::new();
    let device_state = DeviceState::new();
    let device_state_1 = Arc::new(Mutex::new(device_state));
    let device_state_2 = Arc::clone(&device_state_1);
    let device_state_3 = Arc::clone(&device_state_1);

    let iot_shadow_initial = serde_json::to_string(&iot_state_1).unwrap();
    let device_shadow_initial = serde_json::to_string(&*device_state_3.lock().unwrap()).unwrap();

    // If this device is not yet registered execute the fleet provisioning
    if !config
        .iot
        .client_registration_status
        .starts_with(config::IOTREGISTRATIONSTATUS_REGISTERED)
    {
        let registration_status_prior_provisioning = config.iot.client_registration_status.clone();
        match fleet_provisioning::fleet_provisioning().await {
            Ok(IotRegistrationStatus::Registered) => {
                // Re-read config changed at the fleet provisioning
                config = Config::get_config_from_yaml()?;
                // initialize the device shadow in cloud at the first fleet provisioning
                // but not at the certificate rotation fleet provisioning
                if registration_status_prior_provisioning
                    .starts_with(config::IOTREGISTRATIONSTATUS_INITIAL)
                {
                    // set the remote IoTShadow registration status to REGISTERED
                    // send shadow with `reported`
                    iot_shadow_initial_upload =
                        device_shadow::build_shadow_message_payload(iot_shadow_initial, true);
                    device_shadow_initial_upload =
                        device_shadow::build_shadow_message_payload(device_shadow_initial, true);
                } else if registration_status_prior_provisioning
                    .starts_with(config::IOTREGISTRATIONSTATUS_CERTIFICATE_ROTATION_REQUESTED)
                {
                    // set the remote IoTShadow registration status to REGISTERED
                    // send shadow with `desired` and `reported` nodes 
                    // to avoid repeating of the cert rotation in case 
                    // the cloud shadow was not updated quickly
                    iot_shadow_initial_upload =
                        device_shadow::build_shadow_message_payload(iot_shadow_initial, false);
                }
            }
            Ok(e) => {
                let s = format!("Fleet provisioning unfinished - status: {}", e);
                debug!("{}", s);
                return Err(s.into());
            }
            Err(e) => {
                let s = format!("Fleet provisioning error: {}", e);
                debug!("{}", s);
                return Err(s.into());
            }
        }
    }

    // Prepare config for asynchronous multitasking
    let config_1 = Arc::new(config);
    let config_2 = Arc::clone(&config_1);

    let (iot_message_tx_1, mut iot_message_rx): (Sender<IotMessage>, Receiver<IotMessage>) =
        mpsc::channel(CHANNEL_CAPACITY);
    let iot_message_tx_2 = iot_message_tx_1.clone();
    let iot_message_tx_3 = iot_message_tx_1.clone();
    let mut iot_message_tx_4 = iot_message_tx_1.clone();

    // Initiate the IoT connection client
    let aws_settings = ConnectionSettings::new_tls(
        config_1.iot.client_id.to_owned(),
        config_1.iot.endpoint.to_owned(),
        config_1.iot.port.to_owned(),
        config_1.iot.ca_path.to_owned(),
        config_1.iot.client_cert_path.to_owned(),
        config_1.iot.client_priv_key_path.to_owned(),
        None,
    );

    let (iot_core_client, mut iot_eventloop) = AsyncClient::new(aws_settings).await?;

    let iot_client_1 = Arc::new(iot_core_client);
    let iot_client_2 = Arc::clone(&iot_client_1);

    // Initiate shadows (IoT and Device)
    let iot_shadow = device_shadow::DeviceShadow::new(
        config_1.iot.client_id.to_string(),
        ShadowType::Named(config_1.iot.shadow_name.to_string()),
        iot_client_1.get_eventloop_handle(),
    );
    let (iot_shadow_response_tx, mut iot_shadow_response_rx): (
        Tx<serde_json::Value>,
        Rx<serde_json::Value>,
    ) = broadcast::channel(2);
    let iot_descr = DeviceShadowDescriptor::new(
            iot_shadow.get_name(), 
            iot_shadow, 
            iot_shadow_response_tx
        );

    let device_shadow = device_shadow::DeviceShadow::new(
        config_1.iot.client_id.to_string(),
        ShadowType::Named(config_1.device.shadow_name.to_string()),
        iot_client_1.get_eventloop_handle(),
    );
    let (device_shadow_response_tx, mut device_shadow_response_rx): (
        Tx<serde_json::Value>,
        Rx<serde_json::Value>,
    ) = broadcast::channel(2);
    let device_descr = DeviceShadowDescriptor::new(
        device_shadow.get_name(), 
        device_shadow,
        device_shadow_response_tx,
    );

    let mut shadow_descriptors = device_shadow::ShadowDescriptors::new(device_descr, iot_descr);

    // Initiate shadow subscriptions
    shadow_descriptors
        .iot
        .get_shadow_ref()
        .initiate_shadow(
            &mut iot_eventloop.0,
            iot_message_tx_1,
            iot_shadow_initial_upload,
        )
        .await?;
    shadow_descriptors
        .device
        .get_shadow_ref()
        .initiate_shadow(
            &mut iot_eventloop.0,
            iot_message_tx_2,
            device_shadow_initial_upload,
        )
        .await?;

    // Initiate processing data from IoT connection
    let mut iot_receiver = iot_client_1.get_receiver().await;

    let iot_receiver_thread = tokio::spawn(async move {
        loop {
            match iot_receiver.recv().await {
                Ok(event) => match event {
                    Packet::Publish(ev) => {
                        connector_aws::on_iot_event(
                            ev,
                            &mut shadow_descriptors,
                            &mut iot_message_tx_4,
                        )
                        .await?;
                    }
                    _ => {
                        debug!("Got event on iot_receiver: {:?}", event);
                    }
                },
                Err(_) => (),
            }
        }
    });

    let iot_monitor_thread = tokio::spawn(async move {
        match mqtt_client::eventloop_monitor(iot_eventloop).await {
            Ok(_) => Ok::<IotRegistrationStatus, IoTError>(IotRegistrationStatus::Unknown),
            Err(err) => {
                debug!("Error - problem connecting to IoT: {}", err);
                Err(IoTError::AWSConnectionError)
            }
        }
    });

    // Start device specific processing of Device Shadow responses
    let device_shadow_thread = tokio::spawn(async move {
        loop {
            match device_shadow_response_rx.recv().await {
                Ok(val) => match device_state_1.lock() {
                    Ok(mut ds) => {
                        ds.on_receive_device_shadow(&val)?;
                    }
                    Err(_) => (),
                },
                Err(_) => (),
            }
        }
    });

    // Start processing of IoT Shadow responses
    let iot_shadow_thread = tokio::spawn(async move {
        loop {
            match iot_shadow_response_rx.recv().await {
                Ok(val) => match iot_state_1.lock() {
                    Ok(mut is) => {
                        match is.on_receive_iot_shadow(&val) {
                            Ok(IotRegistrationStatus::CertificateRotationRequested) => {
                                return Ok::<IotRegistrationStatus, IoTError>(
                                    IotRegistrationStatus::CertificateRotationRequested,
                                );
                            },
                            Ok(_) => (),
                            Err(_) => (),
                        }
                        debug!("IoT State: {:?}", is)
                    },
                    Err(_) => (),
                },
                Err(_) => (),
            }
        }
    });

    // Initiate the Device connection
    let device_settings = ConnectionSettings::new_tcp(
        config_1.device.client_id.to_owned(),
        config_1.device.endpoint.to_owned(),
        config_1.device.port.to_owned(),
        config_1.device.username.to_owned(),
        config_1.device.password.to_owned(),
        None,
    );

    let (device_client, device_eventloop) = AsyncClient::new(device_settings).await?;

    let device_client_1 = Arc::new(device_client);
    let device_client_2 = Arc::clone(&device_client_1);

    device_client_1
        .subscribe(DEVICE_SUB_TOPIC.to_string(), QoS::AtMostOnce)
        .await
        .unwrap();

    // Initiate processing data from IoT connection
    let mut device_receiver = device_client_1.get_receiver().await;

    let device_receiver_thread = tokio::spawn(async move {
        loop {
            match device_receiver.recv().await {
                Ok(event) => match event {
                    Packet::Publish(event) => {
                        let mut res: Option<IotMessage> = None; 
                        match device_state_2.lock() {
                            Ok(ds) => match iot_state_2.lock() {
                                Ok(is) => 
                                    res = device_adapter::on_device_event(
                                        event, 
                                        &config_2, 
                                        &ds,
                                        &is
                                    ),
                                Err(_) => (),
                            },
                            // Err(_) => None,
                            Err(_) => (),
                        };
                        match res {
                            Some(result) => {
                                iot_message_tx_3.send(result).await.ignore();
                            }
                            None => {
                                warn!("Message or topic malformed or missing");
                            }
                        }
                    }
                    _ => debug!("Got event on device_receiver: {:?}", event),
                },
                Err(_) => (),
            }
        }
    });

    let device_monitor_thread = tokio::spawn(async move {
        match mqtt_client::eventloop_monitor(device_eventloop).await {
            Ok(_) => Ok::<IotRegistrationStatus, IoTError>(IotRegistrationStatus::Unknown),
            Err(err) => {
                debug!("Error - problem connecting to device: {}", err);
                Err(IoTError::DeviceConnectionError)
            }
        }
    });

    // Initiate task for sending messages to the IoT connection
    let iot_transmitter_thread = tokio::spawn(async move {
        loop {
            let iot_message = iot_message_rx.recv().await;
            match iot_message {
                Some(iot_message) => {
                    iot_client_1
                        .publish(
                            iot_message.topic.to_string(),
                            QoS::AtMostOnce,
                            iot_message.message,
                        )
                        .await
                        .unwrap();
                }
                None => {
                    debug!("IoT Receiver task triggered with no content")
                }
            }
        }
    });

    // Process tasks asynchronously until any one exists
    tokio::select!(
        output = device_receiver_thread => {
            return output.unwrap()
        },
        output = device_monitor_thread => {
            iot_client_2.disconnect().await.unwrap();
            return output.unwrap()  // IoTError::DeviceConnectionError
        },
        output = device_shadow_thread => {
            return output.unwrap()
        },
        output = iot_receiver_thread => {
            return output.unwrap()
        },
        output = iot_monitor_thread => {
            device_client_2.disconnect().await.unwrap();
            return output.unwrap()  // IoTError::AWSConnectionError
        },
        output = iot_transmitter_thread => {
            return output.unwrap()
        },
        output = iot_shadow_thread => {
            return output.unwrap()
        },
    );
}

// Main to retry the `mqtt_bridge` on connection errors and if requested invoke it in certification rotation mode
#[tokio::main]
async fn main() {
    // Initiate general functionality, like Logger
    SimpleLogger::new()
        // default level is Warn if not specified in the RUST_LOG env variable
        .with_level(LevelFilter::Warn)
        .with_utc_timestamps()
        .env()
        .init()
        .unwrap();

    // Try to run the MQTT bridge multiple times using either the ExponentialBackoff or the FibonacciBackoff
    let retry_strategy = FibonacciBackoff::from_millis(1000)
        .factor(2)
        .max_delay(Duration::new(120, 0))
        .take(20); // limit to 20 retries

    let mqtt_bridge_run = || async move {
        loop {
            let result = mqtt_bridge().await;
            debug!("MQTT bridge END result: {:?}", result);
            match result {
                // if CertificateRotationRequested set the config status and run once again
                Ok(IotRegistrationStatus::CertificateRotationRequested) => {
                    let mut config =
                        Config::get_config_from_yaml().expect("Problem reading \"config.yaml\"");
                    config.iot.client_registration_status =
                        IotRegistrationStatus::CertificateRotationRequested.to_string();
                    config
                        .store_config_to_yaml()
                        .expect("Problem storing \"config.yaml\"");
                    continue;
                }
                _ => return result,
            }
        }
    };

    let res = Retry::spawn(retry_strategy, mqtt_bridge_run).await;
    debug!("Main END result: {:?}", res);
}
