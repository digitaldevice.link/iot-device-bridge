extern crate iot_device_bridge;
use iot_device_bridge::config::IotRegistrationStatus;
use iot_device_bridge::fleet_provisioning::fleet_provisioning;

use log::LevelFilter;
use log::{debug, error, info};
use simple_logger::SimpleLogger;

use tokio::sync::mpsc::{self, Receiver, Sender};

#[tokio::main]
async fn main() {
    SimpleLogger::new()
        .with_level(LevelFilter::Debug) // default level is Debug if not specified in the RUST_LOG env variable
        .env()
        .with_utc_timestamps()
        .init()
        .unwrap();

    let (tx, mut rx): (
        Sender<IotRegistrationStatus>,
        Receiver<IotRegistrationStatus>,
    ) = mpsc::channel(1);

    tokio::spawn(async move {
        match fleet_provisioning().await {
            Ok(r) => {
                debug!("IotRegistrationStatus: {}", r);
                tx.send(r).await.expect("Task send failed");
            }
            Err(e) => {
                error!("Fleet Provisioning error: {}", e);
                tx.send(IotRegistrationStatus::Unknown)
                    .await
                    .expect("Task send failed");
            }
        }
    });

    if let Some(r) = rx.recv().await {
        info!(
            "Fleet Provisioning FINISHED -- IotRegistrationStatus: {}",
            r
        );
    }
}
