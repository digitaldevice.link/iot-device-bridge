#!/bin/bash
# build-deployment-package.sh prepares deployment folder
# exit when any command fails
set -e 
docker pull -a registry.gitlab.com/knafel/aws-iot-device-bridge && echo SUCCESS || echo FAIL
docker tag registry.gitlab.com/knafel/aws-iot-device-bridge:latest iot-device-bridge:latest && echo SUCCESS || echo FAIL
docker save --output iot-device-bridge.tar iot-device-bridge && echo SUCCESS || echo FAIL
mv iot-device-bridge.tar ./deployment_package/ && echo SUCCESS || echo FAIL
zip -u -r iot-device-bridge.zip deployment_package/* && echo SUCCESS || echo FAIL