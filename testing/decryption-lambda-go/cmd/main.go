/*
Lambda for testing of the data payload encryption using ECIES (Elliptic Curve Integrated Encryption Scheme)
built upon AES-GCM-256 and HKDF-SHA256 and using the secp256k1 curve.
Standardized as: ISO/IEC 18033-2

[1] Expects the decryption key stored in the SecretManager

[2] Expects cloudevents.io compatible events with extensions like below
    (correlation node omitted for clarity and compatibility):

```json
	{
	"confidentialityrelevant": true,
	"data": "BKSpUPrKden4r6hJ4JItIwTTnBSccICzEZIFLENOfvf3fTxYGEF5lcVbjHVFfak8/vdAQRTr1hFrYNQ2O5g10u4/qpqkPJSvuBcYX/UrwgI9gidUUzmiTE3tgzywSdtXxVzjk53u8mjVyQjcfNzEEUFtSxsf7YmFNbOl9hJHKOrvxNhm9dOPOimrgn47hHn8WoGat3PTXlCiiY0lp2vRud6yhTX2J+WuR7HNj+gA5Oj7w37BLTfuVkQgC+yDxK0vnbqNpC/izJWVnXyX3PvQ23oIaA6CXiq32UKohNwTMleMhebqTTReEOlzg1PxsRV1WANft+CO+siZ4nuQO/ip52QAqrC9IuNStEBTVbJKsH7X5/K+vV+IPL9uoOi9UtWq9hjckhbvo9dTijk4I3Y0TKbDjtJNEPn5Z6X9Sn6rAckU6LA59XJn+8bJ+5YcrT7bWEY3lkthRfVtZY1DjUY+nnhymLjcsniNn+ljgUsgtB995YGYMYaxS0u+J6+nxg9MU38gR7u523sqiuCXmaO1YqRnUOYiYNpq6UT7IWIA1+wxeg==",
	"id": "41bb5a42-1f4b-4808-bfeb-f261fced3c28",
	"privacyrelevant": false,
	"source": "urn:rudi:GTIN^7613336170076^99998",
	"specversion": "1.0",
	"time": "2022-06-19T14:07:24.296496719+00:00",
	"type": "ProcessExecution/Run/Started"
	}
```

    delivered by,. e.g, IoT Rule transforming the events

```sql
	SELECT
		confidentialityrelevant,
		data,
		id,
		privacyrelevant,
		source,
		specversion,
		time,
		type
	FROM '<DEVICE_SPECIFIC_TOPIC>/#'
```
*/

package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"log"

	runtime "github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/aws/aws-sdk-go/service/secretsmanager"

	ecies "github.com/ecies/go/v2"
)

const secretName = "iot/dataencrypt/keys"
const region = "eu-central-1"

type IotEvent struct {
	ConfidentialityRelevant bool   `json:"confidentialityrelevant"`
	Data                    string `json:"data"`
	Id                      string `json:"id"`
	PrivacyRelevant         bool   `json:"privacyrelevant"`
	Source                  string `json:"source"`
	SpecVersion             string `json:"specversion"`
	Time                    string `json:"time"`
	EventType               string `json:"type"`
}

type Secret struct {
	SecretKey string `json:"secret_key"`
	PublicKey string `json:"public_key"`
}

// decodePrivateKey from the Base 64 into ECIES library format
func decodePrivateKey(secretString string) (*ecies.PrivateKey, error) {

	secret := Secret{}
	err := json.Unmarshal([]byte(secretString), &secret)
	if err != nil {
		return nil, err
	}

	keyB, err := base64.StdEncoding.DecodeString(string(secret.SecretKey))
	if err != nil {
		return nil, err
	}

	key := ecies.NewPrivateKeyFromBytes(keyB)

	return key, nil
}

var client = lambda.New(session.New())

func getSecret() (*ecies.PrivateKey, error) {

	//Create a Secrets Manager client
	sess, err := session.NewSession()
	if err != nil {
		// Handle session creation error
		log.Printf("ERROR: %v", err.Error())
		return nil, err
	}
	svc := secretsmanager.New(sess,
		aws.NewConfig().WithRegion(region))
	input := &secretsmanager.GetSecretValueInput{
		SecretId:     aws.String(secretName),
		VersionStage: aws.String("AWSCURRENT"),
	}

	result, err := svc.GetSecretValue(input)

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case secretsmanager.ErrCodeDecryptionFailure:
				// Secrets Manager can't decrypt the protected secret text using the provided KMS key.
				log.Printf("ERROR: %v -- %v", secretsmanager.ErrCodeDecryptionFailure, aerr.Error())

			case secretsmanager.ErrCodeInternalServiceError:
				// An error occurred on the server side.
				log.Printf("ERROR: %v -- %v", secretsmanager.ErrCodeInternalServiceError, aerr.Error())

			case secretsmanager.ErrCodeInvalidParameterException:
				// You provided an invalid value for a parameter.
				log.Printf("ERROR: %v -- %v", secretsmanager.ErrCodeInvalidParameterException, aerr.Error())

			case secretsmanager.ErrCodeInvalidRequestException:
				// You provided a parameter value that is not valid for the current state of the resource.
				log.Printf("ERROR: %v -- %v", secretsmanager.ErrCodeInvalidRequestException, aerr.Error())

			case secretsmanager.ErrCodeResourceNotFoundException:
				// We can't find the resource that you asked for.
				log.Printf("ERROR: %v -- %v", secretsmanager.ErrCodeResourceNotFoundException, aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			log.Printf("ERROR: %v", err.Error())
		}
		return nil, err
	}

	// Decrypts secret using the associated KMS key.
	// Depending on whether the secret is a string or binary, one of these fields will be populated.
	if result.SecretString != nil {
		return decodePrivateKey(*result.SecretString)
	} else {
		decodedBinarySecretBytes := make([]byte, base64.StdEncoding.DecodedLen(len(result.SecretBinary)))
		len, err := base64.StdEncoding.Decode(decodedBinarySecretBytes, result.SecretBinary)
		if err != nil {
			log.Printf("ERROR: %v -- %v", "Base64 Decode Error:", err)
			return nil, err
		}
		return decodePrivateKey(string(decodedBinarySecretBytes[:len]))
	}
}

func decryptString(privateKey *ecies.PrivateKey, ciphertextBase64 string) (plaintext string, err error) {

	encryptedB, err := base64.StdEncoding.DecodeString(string(ciphertextBase64))
	if err != nil {
		return "", err
	}

	plaintextB, err := ecies.Decrypt(privateKey, encryptedB)
	if err != nil {
		return "", err
	}
	plaintext = string(plaintextB)

	return plaintext, nil
}

var privateKey *ecies.PrivateKey

func init() {
	var err error
	privateKey, err = getSecret()
	if err != nil {
		panic(err)
	}
}

func handleRequest(ctx context.Context, event IotEvent) (string, error) {

	eventJson, err := json.Marshal(event)
	if err != nil {
		return "", err
	}

	log.Printf("INPUT EVENT (json): %s", eventJson)

	if event.ConfidentialityRelevant {

		plaintext, err := decryptString(privateKey, event.Data)
		if err != nil {
			return "", err
		}

		event.Data = plaintext
		eventJson, err = json.Marshal(event)
		if err != nil {
			return "", err
		}
	}

	log.Printf("OUTPUT EVENT (json): %s", eventJson)
	return string(eventJson), nil
}

func main() {
	runtime.Start(handleRequest)
}
