package main

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/aws"
	v4 "github.com/aws/aws-sdk-go-v2/aws/signer/v4"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/iotdataplane"
	"github.com/aws/aws-sdk-go-v2/service/s3"

	"log"
	"os"
)

const RETRIEVE_OPERATION = "GET"
const STORE_OPERATION = "PUT"

// FileXferInfo defines the request for the file transfer.
// Remark: The Location should not be the name of the bucket as this may change between environments
//         and therefore should be transparent for the device.
//         It should be rather a designation as a key to a mapping (key:value) to the bucket name.
type FileXferInfo struct {
	Operation     string `json:"operation"`     // (required) retrieve (get) or store (put)
	Location      string `json:"location"`      // (optional) if specified designates key to be used in mapping to bucket name
	FilePath      string `json:"filepath"`      // (required) specifying object prefix and key
	ReplyUrlTo    string `json:"replyUrlTo"`    // (required) device-specific response topic, where the lambda shall respond
	ReplyStatusTo string `json:"replyStatusTo"` // (required) status response topic, where the lambda shall respond
}

type IotEvent struct {
	ConfidentialityRelevant bool         `json:"confidentialityrelevant"`
	Data                    FileXferInfo `json:"data"`
	Id                      string       `json:"id"`
	PrivacyRelevant         bool         `json:"privacyrelevant"`
	Source                  string       `json:"source"`
	SpecVersion             string       `json:"specversion"`
	Time                    string       `json:"time"`
	EventType               string       `json:"type"`
}

var (
	s3Bucket        string
	presignS3Client *s3.PresignClient
	iotdataClient   *iotdataplane.Client
	eventType       string
)

// Helper function to read an environment or return a default value
func getEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}

	return defaultVal
}

// Helper function to read an environment variable into integer or return a default value
func getEnvAsInt(name string, defaultVal int) int {
	valueStr := getEnv(name, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}

	return defaultVal
}

func init() {

	region := getEnv("REGION", "eu-central-1")

	s3Bucket = getEnv("DEFAULT_S3_BUCKET", "event-hub-file-transfer")

	eventType = getEnv("EVENT_TYPE_PRESIGNED_URL_REQ", "apis/iot/presigned-s3-url-requests/v1")

	urlExpiration := getEnvAsInt("URL_EXPIRATION_DURATION", 900)

	cfg, err := config.LoadDefaultConfig(context.TODO(),
		config.WithRegion(region),
	)
	if err != nil {
		panic("Failed to load configuration")
	}

	presignS3Client = s3.NewPresignClient(
		s3.NewFromConfig(cfg),
		s3.WithPresignExpires(time.Duration(urlExpiration)*time.Second),
	)

	iotdataClient = iotdataplane.NewFromConfig(cfg)

}

func handleRequest(ctx context.Context, event IotEvent) (string, error) {

	var bucket string
	var presignResult *v4.PresignedHTTPRequest
	var err error
	var status string = "OK"

	// Validate event
	if event.EventType != eventType {
		return "", errors.New("incorrect event type")
	}

	// Get bucket name from the incoming event or from env variable
	if event.Data.Location != "" {
		bucket = event.Data.Location // refactor to retrieve from mapping
	} else {
		bucket = s3Bucket
	}

	// Differentiate handling for GET and PUT operations
	switch event.Data.Operation {
	case STORE_OPERATION:
		presignResult, err = presignS3Client.PresignPutObject(
			context.TODO(), &s3.PutObjectInput{
				Bucket: aws.String(bucket),
				Key:    aws.String(event.Data.FilePath),
			})

		if err != nil {
			status = fmt.Sprintf("ERROR: unable to get presigned URL for PutObject -- %v", err)
			log.Println(status)
		}
		break
	case RETRIEVE_OPERATION:
		presignResult, err = presignS3Client.PresignGetObject(
			context.TODO(), &s3.GetObjectInput{
				Bucket: aws.String(bucket),
				Key:    aws.String(event.Data.FilePath),
			})

		if err != nil {
			status = fmt.Sprintf("ERROR: unable to get presigned URL for GetObject -- %v", err)
			log.Println(status)
		}
		break
	}

	// REMOVE it after testing to avoid publishing security relevant info in the logs
	log.Printf("Presigned URL for [%s]: %s\n", event.Data.FilePath, presignResult.URL)

	// Publish the URL as a response
	if status == "OK" {
		_, err = iotdataClient.Publish(
			context.TODO(), &iotdataplane.PublishInput{
				Topic:   aws.String(event.Data.ReplyUrlTo),
				Payload: []byte(presignResult.URL),
				Qos:     0,
				Retain:  false,
			},
		)
		if err != nil {
			status = fmt.Sprintf("ERROR: unable to Publish URL to IoT Data -- %v", err)
			log.Println(status)
		}
	}

	// Publish the STATUS as a response
	_, err = iotdataClient.Publish(
		context.TODO(), &iotdataplane.PublishInput{
			Topic:   aws.String(event.Data.ReplyStatusTo),
			Payload: []byte(status),
			Qos:     0,
			Retain:  false,
		},
	)
	if err != nil {
		status = fmt.Sprintf("ERROR: unable to Publish STATUS to IoT Data -- %v", err)
		log.Println(status)
	}

	return status, nil
}

func main() {
	lambda.Start(handleRequest)
}
